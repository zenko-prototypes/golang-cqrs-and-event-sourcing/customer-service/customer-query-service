package main

import (
	"CustomerQueryServide/elasticsearch"
	customersv1 "CustomerQueryServide/gen/go/customers/v1"
	"CustomerQueryServide/server"
	"CustomerQueryServide/service"
	"log"
)

func main() {
	s := server.New()

	es, err := elasticsearch.Connect()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	s.RegisterService(&customersv1.CustomersService_ServiceDesc, &service.CustomerServer{EsClient: es})

	err = s.ListenAndServeWithGraceful(":8080")
	if err != nil {
		log.Fatal(err)
	}
}
