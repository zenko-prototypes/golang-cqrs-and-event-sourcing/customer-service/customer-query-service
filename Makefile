# INSTALL

install: ## Install the dependencies
	@echo "Install dependencies"

# BUILD

build: clean ## Build the application
	@echo "Building..."
	@go build -o bin/main main.go
	@echo "Build complete"

# RUN

dev: ## Run the application in development mode
	@echo "Start dev"
	@go run main.go

run: ## Run the application
	@echo "Start run"
	./bin/main

# LINTERS

lint-proto: ## Lint the proto files
	@echo "Lint protobuf file"
	buf lint

# TEST

test: ## Run the tests
	@echo "Start test"
	go test ./...

test-verbose: ## Run the tests in verbose mode
	@echo "Start test in verbose mode"
	go test -v ./...

# GENERATE

generate-protoc: ## Generate code from protobuf file
	@echo "Generate code from protobuf file"
	buf generate

# UTILS

clean: ## Clean old build
	@echo "Remove old build if exist"
	@rm -rf "./bin"
	@mkdir "bin"

help: ## Show this help message.
	@echo 'usage: make [target] ...'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'