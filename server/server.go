package server

import (
	"google.golang.org/grpc"
	"net"
)

type Server struct {
	listener   net.Listener
	grpcServer *grpc.Server
}

func New() *Server {
	return &Server{
		grpcServer: grpc.NewServer(),
	}
}
