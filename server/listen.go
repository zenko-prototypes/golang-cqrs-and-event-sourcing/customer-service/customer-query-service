package server

import (
	"log"
	"net"
)

func (s *Server) Listen(port string) error {
	listener, err := net.Listen("tcp", port)
	if err != nil {
		return err
	}
	s.listener = listener
	return nil
}

func (s *Server) Serve() error {
	log.Printf("Listening on port %s\n", s.listener.Addr())
	return s.grpcServer.Serve(s.listener)
}

func (s *Server) ListenAndServe(port string) error {
	err := s.Listen(port)
	if err != nil {
		return err
	}
	return s.Serve()
}

func (s *Server) ListenAndServeWithGraceful(port string) error {
	s.graceful()
	return s.ListenAndServe(port)
}
