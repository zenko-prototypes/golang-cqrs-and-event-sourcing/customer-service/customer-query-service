package service

import (
	pb "CustomerQueryServide/gen/go/customers/v1"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"github.com/elastic/go-elasticsearch/v7"
	"io"
	"log"
	"strings"
)

type CustomerServer struct {
	EsClient *elasticsearch.Client
}

type Response struct {
	ID     string   `json:"_id"`
	Source Customer `json:"_source"`
}

type Customer struct {
	Name string
}

// TODO: return the customer array

func (s *CustomerServer) SearchCustomers(ctx context.Context, req *pb.SearchCustomersRequest) (*pb.SearchCustomersResponse, error) {

	res, err := s.EsClient.Search(s.EsClient.Search.WithIndex("customer"))
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			err = errors.New("error closing search customer response")
		}
	}(res.Body)

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			// Print the response status and error information.
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	var r map[string]interface{}

	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	// Print the response status, number of results, and request duration.
	log.Printf(
		"[%s] %d hits; took: %dms",
		res.Status(),
		int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
		int(r["took"].(float64)),
	)
	// Print the ID and document source for each hit.
	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		log.Printf(" * ID=%s, %s", hit.(map[string]interface{})["_id"], hit.(map[string]interface{})["_source"])
	}

	log.Println(strings.Repeat("=", 37))
	return nil, nil
}
func (s *CustomerServer) GetCustomer(ctx context.Context, req *pb.GetCustomerRequest) (*pb.GetCustomerResponse, error) {
	id := req.GetId()
	if id == "" {
		return nil, errors.New("Empty id")
	}

	res, err := s.EsClient.Get("customer", id)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(res.Body)
	if err != nil {
		return nil, err
	}
	b := buf.Bytes()

	customer := &Response{}
	err = json.Unmarshal(b, customer)
	if err != nil {
		return nil, err
	}

	return &pb.GetCustomerResponse{
		Customer: &pb.Customer{
			Id:   customer.ID,
			Name: customer.Source.Name,
		},
	}, nil
}
